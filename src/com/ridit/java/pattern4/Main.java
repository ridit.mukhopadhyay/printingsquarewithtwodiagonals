package com.ridit.java.pattern4;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		System.out.println("Enter the length of the square");
		int length = in.nextInt();
		for(int row = 0;row<length;row++) {
			if((row == 0) ||  (row == length - 1)){
				printTopAndBottom(length);
			}
			else {
				printRow(row, length);
			}
			
			
		}
		
		
		

	}
	static void printRow (int rownumber,int length) {
		for(int column = 0; column<length;column++) {
			if((column == 0) || (column == length -1) ||(column == rownumber) || (column == length - 1 - rownumber)) {
				System.out.print("*" + "\t" );
			}
			
			else {
				System.out.print("\t");
			}	
		}
		
		System.out.println();
		System.out.println();
		System.out.println();
	}
	
	static void printTopAndBottom(int length) {
		for(int column = 0; column<length ; column++) {
			System.out.print("*" + "\t");
		}
		System.out.println();
		System.out.println();
		System.out.println();
	}
	
	

}
